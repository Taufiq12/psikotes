<div class="row" id="body-row">
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
        <ul class="list-group">
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small style="color: white;">MENU</small>
            </li>
            <a href="?hal=home" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-home fa-fw mr-3" style="color: white;"></span>
                    <span class="menu-collapsed" style="color: white;">Beranda</span>
                </div>
            </a>
            <a href="?hal=profiluser" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-user fa-fw mr-3" style="color: white;"></span>
                    <span class="menu-collapsed" style="color: white;">Profil Peserta</span>
                </div>
            </a>
            <!-- <a href="?hal=soal" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-file fa-fw mr-3" style="color: white;"></span>
                    <span class="menu-collapsed" style="color: white;">Soal</span>
                </div>
            </a>    -->
            <a href="href=../../logout.php" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out-alt fa-fw mr-3" style="color: white;"></span>
                    <span class="menu-collapsed" style="color: white;">Keluar</span>
                </div>
            </a>    
        </ul>
    </div> <!-- End Sidebar -->

    <!-- MAIN -->
    <div class="col">
<!-- Page Content -->
<div id="page-wrapper">
            <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-lg-12">
                      <!--   <h3 class="page-header"> Peraturan </h3> -->

                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                    
                        <div class="card-header bg-danger text-white">
                           Peraturan
                        </div>
                        <div class="card-body">
                          
<?php
$tanggal = mktime(date('m'), date("d"), date('Y'));
echo "<h5 class='ml-4' align='center'>Tanggal : <b> " . date("d-m-Y", $tanggal ) . "</b>";
date_default_timezone_set("Asia/Jakarta");
$jam = date ("H:i:s");
echo " | Pukul : <b> " . $jam . " " ." </b> ";
$a = date ("H");

if($_SESSION[jk] == "Pria"){
    $title = "Bapak";
}else{
    $title = "Ibu";
}

if (($a>=6) && ($a<11)) {
    echo " <b>, Selamat Pagi $title $_SESSION[username]  <i class='fas fa-sun'></i></b>";
}else if(($a>=11) && ($a<15)){
    echo " , Selamat  Siang $title $_SESSION[username] <i class='fas fa-sun'></i>";
}elseif(($a>=15) && ($a<19)){
    echo ", Selamat Sore $title $_SESSION[username] <i class='fas fa-cloud-moon'></i>";
}else{
    echo ", <b> Selamat Malam $title $_SESSION[username] <i class='fas fa-moon'></i></b></h5>";
}

$qrydiv=mysql_query("SELECT * FROM tbl_user WHERE id_user='$_SESSION[iduser]'");
$rdiv=mysql_fetch_array($qrydiv);

$qry=mysql_query("SELECT * FROM tbl_pengaturan_tes");
$r=mysql_fetch_array($qry);

$tampildiv = mysql_query("SELECT * FROM tbl_division  WHERE id_division=$r[id_division]");
$tdiv=mysql_fetch_array($tampildiv);

$curtime =  date ("H:i:s");
$endtime =  date("H:i:s", strtotime($r['end_time']));
$to_time = strtotime($curtime);
$from_time = strtotime($endtime);
$sisawaktu = round(abs($to_time - $from_time) / 60,2);

$jml_soal = $r['jml_soal'];

$result=mysql_query("select * from tbl_soal WHERE aktif='Y' AND id_division = '$rdiv[id_division]' ORDER BY RAND () LIMIT $jml_soal ");
$hitung=mysql_num_rows($result);

$start_time =  date("H:i", strtotime($r['start_time']));
$end_time =  date("H:i", strtotime($r['end_time']));

		echo "
        <h3 align='center'>$r[nama_tes]</h3><hr/><br/>
        <i class='fas fa-clock mr-2'></i>Jam Mulai Pengerjaan : $start_time<br/>
        <i class='fas fa-clock mr-2'></i>Jam Berakhir Pengerjaan : $end_time<br/>

        <i class='fas fa-clock mr-2'></i>Waktu Pengerjaan : $r[waktu] menit<br/>
		<i class='fas fa-file-alt mr-2'></i>Jumlah Soal : $hitung<br/>
		<p/>
		<h2>PERATURAN</h2><br/>
		$r[peraturan]<br/>
        ";
        $currDate = date('Y-m-d');
        $tgl =  $r[tgl_pengerjaan];

        $curr_time =  date("H:i", strtotime($jam));


    if ($r[id_division] == $rdiv[id_division] OR $tdiv[division] == "ALL")
    {   
        if ($currDate = $tgl)
        {
            if($currDate = $tgl && $curr_time > $start_time && $curr_time < $end_time )
            {
                echo "<form name=fValidate>
                <input type=checkbox name=agree id=agree value=1> Saya Mengerti dan Siap Untuk Mengikuti Tes<br/><br/>
                    <div style=text-align:center;>
                        <input type='button' name='button-ok' class='btn btn-primary' value='MULAI TES' onclick='cekForm()'>
                    </div>
                </form>";

                if($sisawaktu <= $r['waktu'])
                {
                    mysql_query("UPDATE tbl_pengaturan_tes SET waktu = round($sisawaktu)"); 
                }

            }elseif($currDate = $tgl && $curr_time > $start_time && $curr_time > $end_time )
            {
                echo "Pengerjaan Tes sudah berakhir!"; 
            }
            else
            {
                echo "Pengerjaan Tes Belum dimulai!";
            }
        }
        else
        {
            echo "Pengerjaan Tes sudah berakhir!";  
        }
    }else{
        echo "Saat ini belum ada pengerjaan tes!"; 
    }
        
?>
<script>
 function cekForm() {
	if (!document.fValidate.agree.checked) {
				alert("Anda belum menyetujui!");
				return false;
			} else {
				location.href="?hal=soal";
			}
		}
</script>



                        </div>
                    </div>
                    </div>    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    </div>
               


