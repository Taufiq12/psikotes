<?php
$conn = mysqli_connect("localhost:3307","root","11223344","psikotes");
require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
    
//   $tgl = date("Y-m-d");
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
  
  if(in_array($_FILES["file"]["type"],$allowedFileType)){

        $targetPath = 'uploads/'.$_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
        
        $Reader = new SpreadsheetReader($targetPath);
        
        $sheetCount = count($Reader->sheets());
        for($i=0;$i<$sheetCount;$i++)
        {
            
            $Reader->ChangeSheet($i);
            
            foreach ($Reader as $Row)
            {
          
                $soal = "";
                if(isset($Row[0])) {
                    $soal = mysqli_real_escape_string($conn,$Row[0]);
                }
                
                $a = "";
                if(isset($Row[1])) {
                    $a = mysqli_real_escape_string($conn,$Row[1]);
                }

                $b = "";
                if(isset($Row[2])) {
                    $b = mysqli_real_escape_string($conn,$Row[2]);
                }

                $c = "";
                if(isset($Row[3])) {
                    $c = mysqli_real_escape_string($conn,$Row[3]);
                }

                $d = "";
                if(isset($Row[4])) {
                    $d = mysqli_real_escape_string($conn,$Row[4]);
                }

                $knc_jawaban = "";
                if(isset($Row[5])) {
                    $knc_jawaban = mysqli_real_escape_string($conn,$Row[5]);
                }

                $division = "";
                if(isset($Row[6])) {
                    $division = mysqli_real_escape_string($conn,$Row[6]);
                }
                
                if (!empty($soal) || !empty($soal)) {

                    if ($soal != "soal" && $a != "Jawaban A" && $b != "Jawaban B" && $c != "Jawaban C" && $d != "Jawaban D")
                    {

                        $result = mysqli_query($conn, "select * from tbl_soal where soal = '".$soal."' ");
                        $rowcount=mysqli_num_rows($result); 

                        $result = mysqli_query($conn, "select * from tbl_pengaturan_tes");
                        $r=mysqli_fetch_array($result);

                        $query = mysqli_query($conn, "select * from tbl_division where division = '".$division."'");
                        $t=mysqli_fetch_array($query);
            
                        if($rowcount > 0){ 
                            $query = "update tbl_soal set soal = '".$soal."', a = '".$a."', b = '".$b."', c = '".$c."', d = '".$d."', knc_jawaban = '".$knc_jawaban."', aktif = 'Y', tanggal = '".$r[tgl_pengerjaan]."', id_division = '".$t[id_division]."' where soal = '".$soal."'";
                            $result = mysqli_query($conn, $query);
                        }
                        else
                        { 
                            $query = "insert into tbl_soal(soal, a, b, c, d, knc_jawaban, aktif, tanggal, id_division) values('".$soal."', '".$a."', '".$b."', '".$c."', '".$d."', '".$knc_jawaban."', 'Y', '".$r[tgl_pengerjaan]."' , '".$t[id_division]."')";
                            $result = mysqli_query($conn, $query);
                        }
                    
                        if (! empty($result)) {
                            $type = "success";
                            $message = "Excel Data Imported into the Database";
                            header('location:admin/media?module=soal');
                            unlink($targetPath);
                        } else {
                            $type = "error";
                            $message = "Problem in Importing Excel Data";
                            header('location:admin/media?module=soal');
                            unlink($targetPath);
                        }
                    }
                }
             }
        
         }
  }
  else
  { 
        $type = "error";
        $message = "Invalid File Type. Upload Excel File.";
        header('location:admin/media?module=users');
  }
}
?>