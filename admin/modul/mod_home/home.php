<?php 
session_start();
include "../../../config/koneksi.php";
include "../../../config/fungsi_thumb.php";

$module=$_GET[module];
$act=$_GET[act];

$sql  = mysql_query("SELECT * FROM tbl_pengaturan_tes");
$r    = mysql_fetch_array($sql);


$tampildiv = mysql_query("SELECT * FROM tbl_division  WHERE id_division=$r[id_division]");
$tdiv=mysql_fetch_array($tampildiv);

if($tdiv[division] == "ALL")
{
    $sql_nilai_l  = mysql_query("SELECT count(*) as jum FROM tbl_nilai WHERE Tanggal = '$r[tgl_pengerjaan]'");
    $r_nilai_l    = mysql_fetch_array($sql_nilai_l);
    $baik=$r_nilai_l['jum'];

    $sql_nilai_2  = mysql_query("SELECT count(*) as jum 
                                FROM tbl_user a
                                INNER JOIN (SELECT DISTINCT id_division FROM tbl_soal WHERE aktif='Y' AND  Tanggal = '$r[tgl_pengerjaan]') b ON a.id_division = b.id_division
                                WHERE  id_user NOT IN (SELECT id_user FROM tbl_nilai WHERE Tanggal = '$r[tgl_pengerjaan]')");
    $r_nilai_2    = mysql_fetch_array($sql_nilai_2);
    $tidak=$r_nilai_2['jum'];

    $sql_user  = mysql_query("SELECT count(*) as jum FROM tbl_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division");
    $r_user    = mysql_fetch_array($sql_user);
    $total_user=$r_user['jum'];

    $sql_soal  = mysql_query("SELECT count(*) as jum FROM tbl_soal WHERE aktif='Y' AND  Tanggal = '$r[tgl_pengerjaan]'");
    $r_soal    = mysql_fetch_array($sql_soal);
    $total_soal=$r_soal['jum'];

}else{

    $sql_nilai_l  = mysql_query("SELECT count(*) as jum FROM tbl_nilai WHERE Tanggal = '$r[tgl_pengerjaan]'");
    $r_nilai_l    = mysql_fetch_array($sql_nilai_l);
    $baik=$r_nilai_l['jum'];

    $sql_nilai_2  = mysql_query("SELECT count(*) as jum 
                                FROM tbl_user a
                                INNER JOIN (SELECT DISTINCT id_division FROM tbl_soal WHERE aktif='Y' AND  Tanggal = '$r[tgl_pengerjaan]') b ON a.id_division = b.id_division
                                WHERE  id_user NOT IN (SELECT id_user FROM tbl_nilai WHERE Tanggal = '$r[tgl_pengerjaan]') AND a.id_division = $r[id_division]");
    $r_nilai_2    = mysql_fetch_array($sql_nilai_2);
    $tidak=$r_nilai_2['jum'];

    $sql_user  = mysql_query("SELECT count(*) as jum FROM tbl_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE tbl_division.id_division = $r[id_division]");
    $r_user    = mysql_fetch_array($sql_user);
    $total_user=$r_user['jum'];

    $sql_soal  = mysql_query("SELECT count(*) as jum FROM tbl_soal WHERE aktif='Y' AND  Tanggal = '$r[tgl_pengerjaan]' AND id_division = $r[id_division]");
    $r_soal    = mysql_fetch_array($sql_soal);
    $total_soal=$r_soal['jum'];
}

?>
<!-- Sidebar -->
<div class="row" id="body-row">
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
        <ul class="list-group">
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small style="color: white;">MENU</small>
            </li>
            <a href="?module=home" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-home fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Beranda</span>
                </div>
            </a>
            <a href="?module=soal" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tasks fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Kelola Soal Tes</span>
                </div>
            </a>
            <a href="?module=hasiltes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-file-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Hasil Tes</span>
                </div>
            </a>
            <a href="?module=pengaturantes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tools fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Pengaturan Tes</span>
                </div>
            </a>
            <a href="?module=users" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-users fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Daftar Peserta</span>
                </div>
            </a>       
            <a href="href=../../logout.php" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Keluar</span>
                </div>
            </a>
            <a href="?module=pengguna" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Pengguna</span>
                </div>
            </a>       
        </ul>
    </div> <!-- Akhir Sidebar -->

    <!-- Header Aplikasi Tes Online -->
    <div class="col">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">            
                <div class="col-lg-12 mt-3">
                    <h3 class="display-4 text-center">e-Tes Online</h3>
                    <hr/>
                </div>
            </div>
        
        <!-- Waktu Tanggal Selamat Admin -->
        <div class="row">
        <div class="container text-center mb-2">
            <?php 
                $tanggal = mktime(date('m'), date("d"), date('Y'));
                echo "<h5 class='ml-4'>Tanggal : <b> " . date("d-m-Y", $tanggal ) . "</b>";
                date_default_timezone_set("Asia/Jakarta");
                $jam = date ("H:i:s");
                echo " | Pukul : <b> " . $jam . " " ." </b> ";
                $a = date ("H");


                if($_SESSION[jk] == "Pria"){
                    $title = "Bapak";
                }else{
                    $title = "Ibu";
                }
                
                if (($a>=6) && ($a<11)) {
                    echo " <b>, Selamat Pagi $title $_SESSION[nama]  <i class='fas fa-sun'></i></b>";
                }else if(($a>=11) && ($a<15)){
                    echo " , Selamat  Siang $title $_SESSION[nama] <i class='fas fa-sun'></i>";
                }elseif(($a>=15) && ($a<19)){
                    echo ", Selamat Sore $title $_SESSION[nama] <i class='fas fa-cloud-moon'></i>";
                }else{
                    echo ", <b> Selamat Malam $title $_SESSION[nama] <i class='fas fa-moon'></i></b></h5>";
                }
            ?>
        </div>
        </div>
    <!-- Header Menu card -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header bg-danger text-white">
                <span class="fas fa-home fa-fw mr-3"></span>Beranda
            </div>
        </div>
    </div> 

    <!-- Isi Card -->
    <div class="card-body">
        <div class="row mt-1">
        <!-- Kotak Peserta Mengerjakan -->
        <div class="col-lg-3 col-md-6">
                <a href="media?module=hasiltes">
            <div class="card text-white">
            <div class="card-header green">
                <div class="row">
                    <div class="col-3">
                        <i class="fa fa-user-check fa-4x mt-2"></i>
                    </div>
                    <div class="col-9 text-right">
                        <div class="font-weight-bold" style="font-size:22px"><?php echo $baik ?></div>
                        <div class="font-weight-light pl-3">Peserta Telah Mengerjakan</div>
                    </div>
                </div>
            </div></a>
            </div>
        </div>
        <!-- Kotak Peserta Belum Mengerjakan -->
        <div class="col-lg-3 col-md-6">
                <a href="media?module=belummengerjakan">
                <div class="card text-white">
                    <div class="card-header light-red">
                        <div class="row">
                            <div class="col-3">
                                <i class="fa fa-user-slash fa-4x mt-2"></i>
                            </div>
                            <div class="col-9 text-right">
                        <div class="font-weight-bold" style="font-size:22px"><?php echo $tidak ?></div>
                        <div class="font-weight-light pl-3">Peserta Belum Mengerjakan</div>
                    </div>
                </div>
            </div></a>
            </div>
        </div>
        <!-- Kotak Total Peserta -->
        <div class="col-lg-3 col-md-6">
            <a href="?module=users">
            <div class="card text-white ">
                <div class="card-header bg-info">
                    <div class="row">
                        <div class="col-3">
                            <i class="fa fa-users fa-4x mt-2"></i>
                        </div>
                        <div class="col-9 text-right">
                            <div class="font-weight-bold" style="font-size:38px"><?php echo $total_user ?></div>
                            <div class="font-weight-light">Total Peserta</div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
        </div>
        <!-- Kotak Total Soal Tes -->
        <div class="col-lg-3 col-md-6">
            <a href="?module=soal">
            <div class="card text-white">
                <div class="card-header color">
                    <div class="row">
                        <div class="col-3">
                            <i class="fa fa-file-alt fa-4x mt-2"></i>
                        </div>
                        <div class="col-9 text-right">
                            <div class="font-weight-bold" style="font-size:38px"><?php echo $total_soal ?></div>
                            <div class="font-weight-light">Total Soal</div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>    
    </div>
</div>
</div>
</div>
</div>
        