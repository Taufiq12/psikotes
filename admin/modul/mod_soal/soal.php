<!-- Sidebar -->
<div class="row" id="body-row">
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
        <ul class="list-group">
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small style="color: white;">MENU</small>
            </li>
            <a href="?module=home" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-home fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Beranda</span>
                </div>
            </a>
            <a href="?module=soal" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tasks fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Kelola Soal Tes</span>
                </div>
            </a>
            <a href="?module=hasiltes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-file-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Hasil Tes</span>
                </div>
            </a>
            <a href="?module=pengaturantes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tools fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Pengaturan Tes</span>
                </div>
            </a>
            <a href="?module=users" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-users fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Daftar Peserta</span>
                </div>
            </a> 
            <a href="href=../../logout.php" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Keluar</span>
                </div>
            </a>          
        </ul>
    </div> <!-- End Sidebar -->

    <!-- MAIN -->
    <div class="col">
    <div id="page-wrapper">
      <div class="container-fluid mt-3">
        <div class="row">
          <div class="col-lg-12">
          </div>    
        </div>    
        <div class="row">
          <div class="col-lg-12">
            <div class="card-header bg-danger text-white">
              Kelola Soal
            </div>
    <div class="card-body">                    
<script language="JavaScript">
function bukajendela(url) {
 window.open(url, "window_baru", "width=800,height=500,left=120,top=10,resizable=1,scrollbars=1");
}
</script>

<?php
session_start();
 if (empty($_SESSION['username']) AND empty($_SESSION['passuser'])){
  echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
  echo "<a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
$aksi="modul/mod_soal/aksi_soal.php";
switch($_GET[act]){
  // Tampil Soal
  default:
  //Template Soal
  echo "<div class='row'><div class='col-lg-6'>
          <a href='../DownloadTemplate/format.xlsx' class='btn btn-primary'> <span class='glyphicon glyphicon-download'></span>Download Format</a></div></div></br></br>";

  //File Upload
  echo "<div class='row'>
          <div class='col-lg-6'>
            <form method='post' enctype='multipart/form-data' action='../uploadsoal.php'>
              Silakan Pilih File Excel: 
              <input type='file' name='file' id='file' accept='.xls,.xlsx'>
              <button type='submit' id='submit' name='import' class='btn btn-primary'>Import</button>
            </form>
          </div>
        </div>
        </br>
        </br>";

  include "config/koneksi.php";
  $sql ="select * from tbl_division" ;// sql to get values from mysql
  $res = mysql_query($sql);
  $value = $r['id_division'];

  // Tombol Tambah Soal
  echo "<div class='row'><div class='col-lg-6'>
  <input class='btn btn-primary' type=button value='Tambah Soal' 
  onclick=\"window.location.href='?module=soal&act=tambahsoal';\"></div>";
  echo "<div col-lg-6>
        <form class='form-inline'method='POST' action=?module=soal&act=carisoal>
        <div class='form-group mx-sm-3 mb-2'>
        <input class='form-control' type=text name='cari'  placeholder='Masukkan Pertanyaan' list='auto' />
        <select name='id_division' id='id_division' class='form-control'>";
        while($row=mysql_fetch_array($res))
        {
          $selected=($row['id_division']==$value)? "selected" : "";
          echo '<option '.$selected.' value="'.$row['id_division'].'" >'.$row['division'].'</option>';
        }
  echo "</select>
        <button class='btn btn-success ml-3' type='submit'><i class='fa fa-search mr-1'></i>Cari</button></div></div>";
    echo"<datalist id='auto'>";
     $qry=mysql_query("SELECT * FROM tbl_soal INNER JOIN tbl_division ON tbl_soal.id_division=tbl_division.id_division");
     while ($t=mysql_fetch_array($qry)) {
      echo "<option value='$t[soal]'>";
     }
  echo"</datalist></form>
      </div>";
  //Tampil Data Soal    
     echo" <table class='table table-hover '>
          <thead><tr align='center'><th>No</th><th>Pertanyaan</th><th>Division</th><th>Tanggal Soal</th><th>Aksi</th><th>Lihat</th></tr></thead>"; 
    
    $sql  = mysql_query("SELECT * FROM tbl_pengaturan_tes");
    $r    = mysql_fetch_array($sql);

    $tampildiv = mysql_query("SELECT * FROM tbl_division  WHERE id_division=$r[id_division]");
    $tdiv=mysql_fetch_array($tampildiv);

    if($tdiv[division] == "ALL")
    {

    session_start();
    $id_paging = $_SESSION['id_paging'];

    // Langkah 1. Tentukan batas,cek halaman & posisi data
    $batas   = 10;
    $halaman = $id_paging;

    if(empty($halaman)){
      $posisi  = 0;
      $halaman = 1;
    }
    else{ 
      $posisi  = ($halaman-1) * $batas; 
    }

    $no = $posisi+1;

    // Langkah 3: Hitung total data dan halaman serta link 1,2,3 
    $query2     = mysql_query("SELECT * FROM tbl_soal INNER JOIN tbl_division ON tbl_soal.id_division=tbl_division.id_division WHERE tbl_soal.aktif='Y' ORDER BY tbl_soal.tanggal DESC");
    $jmldata    = mysql_num_rows($query2);
    $jmlhalaman = ceil($jmldata/$batas);

    // Langkah 2. Sesuaikan query dengan posisi dan batas
    $tampil = mysql_query("SELECT * FROM tbl_soal INNER JOIN tbl_division ON tbl_soal.id_division=tbl_division.id_division WHERE tbl_soal.aktif='Y' ORDER BY tbl_soal.tanggal DESC LIMIT $posisi, $batas");
    }else{

    session_start();
    $id_paging = $_SESSION['id_paging'];

    // Langkah 1. Tentukan batas,cek halaman & posisi data
    $batas   = 10;
    $halaman = $id_paging;

    if(empty($halaman)){
      $posisi  = 0;
      $halaman = 1;
    }
    else{ 
      $posisi  = ($halaman-1) * $batas; 
    }

    // Langkah 2. Sesuaikan query dengan posisi dan batas
    $tampil = mysql_query("SELECT * FROM tbl_soal INNER JOIN tbl_division ON tbl_soal.id_division=tbl_division.id_division WHERE tbl_soal.aktif='Y' AND tbl_soal.id_division = $r[id_division] ORDER BY tbl_soal.tanggal DESC LIMIT $posisi, $batas");   
    
    $no = $posisi+1;

    // Langkah 3: Hitung total data dan halaman serta link 1,2,3 
    $query2     = mysql_query("SELECT * FROM tbl_soal INNER JOIN tbl_division ON tbl_soal.id_division=tbl_division.id_division WHERE tbl_soal.aktif='Y' AND tbl_soal.id_division = $r[id_division] ORDER BY tbl_soal.tanggal DESC");
    $jmldata    = mysql_num_rows($query2);
    $jmlhalaman = ceil($jmldata/$batas);
  }

    echo "<br> Halaman : ";


    for($i=1;$i<=$jmlhalaman;$i++)
    if ($i != $halaman){
      echo " <a href=?module=soal?halaman=$i>$i</a> | ";
    }
    else{ 
      echo " <b>$i</b> | "; 
    }
    echo "<p>Total soal : <b>$jmldata</b></p>";

    while ($r=mysql_fetch_array($tampil)){
    $soal=substr($r[soal],0,50);
       echo "<tr><td>$no</td>
             <td>$soal..</td>
             <td align='center'>$r[division]</td>
             <td align='center'>$r[tanggal]</td>
             <td>
        <a class='btn btn-outline-primary' href=?module=soal&act=editsoal&id=$r[id_soal] role='button'><i class='fa fa-edit mr-1'></i>Edit</a> | 
        <a class='btn btn-outline-danger' href=$aksi?module=soal&act=hapus&id=$r[id_soal] role='button'><i class='fa fa-trash mr-1'></i>Hapus</a></td>
        <td> <a class='btn btn-outline-info' href='?module=soal&act=viewsoal&id=$r[id_soal]' ><i class='fa fa-eye mr-1'></i>Lihat</a></td>";        
        echo"   </td>
    </tr>";
      $no++;
    }
    echo "</table>";
    break;
  
  // Form Tambah Soal
  case "tambahsoal":
    echo "<h2 class'mb-3'><i class='fa fa-plus mr-2'></i>Tambah Soal</h2><hr/>
          <form method=POST class='form-horizontal' action='$aksi?module=soal&act=input' enctype='multipart/form-data'>
                        
        ";
        include "config/koneksi.php";
        $sql ="select * from tbl_division" ;// sql to get values from mysql
        $res = mysql_query($sql);
        
        echo "<div class='form-group'>
              <label for='inputEmail3' class='col-sm-2 control-label'>Division</label>
              <div class='col-sm-4'>
                <select name='id_division' id='id_division' class='form-control'>";

        while($row=mysql_fetch_array($res))
        {
          echo '<option '.$row['id_division'].' value="'.$row['id_division'].'">'.$row['division'].'</option>';
        }

        echo "</select>";

                        echo"
                          </div>
                        </div>
                        
                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Soal</label>
                          <div class='col-sm-10'>
                            <textarea name='soal' style='width: 100%; height: 100%;'></textarea>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Gambar</label>
                          <div class='col-sm-10'>
                            <input type=file name='fupload' size=40> 
                              <br>Tipe gambar harus JPG/JPEG dan ukuran lebar maks: 400 px
                          </div>
                        </div>


                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Jawaban A</label>
                          <div class='col-sm-12'>
                            <input type=text name='a' class='form-control' size=80 required/>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Jawaban B</label>
                          <div class='col-sm-12'>
                            <input type=text name='b' class='form-control' size=80 required/>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Jawaban C</label>
                          <div class='col-sm-12'>
                            <input type=text name='c' class='form-control' size=80 required/>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Jawaban D</label>
                          <div class='col-sm-12'>
                            <input type=text name='d' class='form-control' size=80 required/>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Kunci Jawaban</label>
                          <div class='col-sm-4'>
                            <select name='knc_jawaban' class='form-control'>
                            <option value='a'>A</option>
                            <option value='b'>B</option>
                            <option value='c'>C</option>
                            <option value='d'>D</option>
                            </select>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'></label>
                          <div class='col-sm-4'>
                        <button class='btn btn-primary' type='submit' name='submit'><i class='fa fa-save mr-1'></i>Simpan</button>
                        <input type=button value=Batal onclick=self.history.back() class='btn btn-danger'>
                        </div>
                        </div>
                  </form>";
     break;
  
  // Form Edit Soal  
  case "editsoal":
    $edit=mysql_query("SELECT * FROM tbl_soal INNER JOIN tbl_division ON tbl_soal.id_division=tbl_division.id_division WHERE id_soal='$_GET[id]'");
    $r=mysql_fetch_array($edit);
 
    echo "<h2 class='mb-3'><i class='fa fa-edit mr-2'></i>Edit Soal Tes</h2><hr/>
          <form method=POST action=$aksi?module=soal&act=update class='form-horizontal' enctype='multipart/form-data'>
          <input type=hidden name=id value='$r[id_soal]'>
          ";

        include "config/koneksi.php";
        $sql ="select * from tbl_division" ;// sql to get values from mysql
        $res = mysql_query($sql);
        $value = $r['id_division'];
        
        echo "<div class='form-group'>
              <label for='inputEmail3' class='col-sm-2 control-label'>Division</label>
              <div class='col-sm-4'>
                <select name='id_division' id='id_division' class='form-control'>";

        while($row=mysql_fetch_array($res))
        {
          $selected=($row['id_division']==$value)? "selected" : "";
          echo '<option '.$selected.' value="'.$row['id_division'].'" >'.$row['division'].'</option>';
        }

        echo "</select>";

                        echo"
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Pertanyaan</label>
                          <div class='col-lg-10'>
                            <textarea name='soal' style='width: 100%; height: 100%;'>$r[soal]</textarea>
                          </div>
                        </div>";
                        if ($r[gambar]!=''){

                        echo "
                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'></label>
                          <div class='col-sm-10'>
                            <img src='../foto/$r[gambar]' class='img-thumbnail'>
                          </div>
                        </div>

                        ";  
                        }

                        echo"
                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Gambar</label>
                          <div class='col-sm-10'>
                            <input type=file name='fupload' size=40> 
                                          <br>Tipe gambar harus JPG/JPEG dan ukuran lebar maks: 400 px
                          </div>
                        </div>


                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Jawaban A</label>
                          <div class='col-sm-4'>
                            <input type=text name='a' class='form-control' value='$r[a]' size=80 required/>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Jawaban B</label>
                          <div class='col-sm-4'>
                            <input type=text name='b' value='$r[b]' class='form-control' size=80 required/>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Jawaban C</label>
                          <div class='col-sm-4'>
                            <input type=text name='c' value='$r[c]' class='form-control' size=80 required/>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Jawaban D</label>
                          <div class='col-sm-4'>
                            <input type=text name='d' value='$r[d]' class='form-control' size=80 required/>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'>Kunci Jawaban</label>
                          <div class='col-sm-4'>
                            <select name='knc_jawaban' id='knc_jawaban' class='form-control'>
                            <option ".($r[knc_jawaban] == 'a' ? 'selected' : '')." value='a'>A</option>
                            <option ".($r[knc_jawaban] == 'b' ? 'selected' : '')." value='b'>B</option>
                            <option ".($r[knc_jawaban] == 'c' ? 'selected' : '')." value='c'>C</option>
                            <option ".($r[knc_jawaban] == 'd' ? 'selected' : '')." value='d'>D</option>
                            </select>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label for='inputEmail3' class='col-sm-2 control-label'></label>
                          <div class='col-sm-4'>
                        <button class='btn btn-primary' type='submit' name='submit'><i class='fa fa-save mr-1'></i>Simpan</button>
                        <input type=button value=Batal onclick=self.history.back() class='btn btn-danger'>
                        </div>
                        </div>

        </form>";
    break;  
  
  case "viewsoal":
    $view=mysql_query("SELECT * FROM tbl_soal INNER JOIN tbl_division ON tbl_soal.id_division=tbl_division.id_division WHERE id_soal='$_GET[id]'");
    $t=mysql_fetch_array($view);
    echo "<h2><i class='fa fa-eye mr-2'></i>Detail Soal</h2><hr>
    <div class='container'>
    <a class='btn btn-success mb-4' href='?module=soal'>Kembali</a>
    <h5>Division - $t[division]</h5> 
    <h5>Soal Pertanyaan :</h5>
    $t[soal]</br>";
          if ($t[gambar]!=''){
              echo "<img src='../foto/$t[gambar]' class='img-thumbnail mt-2 mb-2'>";  
          }
    echo"<h5>Jawaban :</h5>
    a. $t[a] </br>
    b. $t[b] </br>
    c. $t[c] </br>
    d. $t[d] </br>";
    echo "<h5>Kunci Jawaban : $t[knc_jawaban]</h5> 
    </div>";
    
  break;
  
  case "carisoal":
       echo"<h2><i class='fa fa-search mr-3'></i>Hasil Pencarian</h2><hr/>
       <a class='btn btn-success mt-1 mb-1' href='?module=soal' role='button'><i class='fa fa-sign-out-alt mr-1'></i>Kembali</a>
     <table class='table table-hover mt-3'>
          <thead><tr align='center'><th>No</th><th>Pertanyaan</th><th>Division</th><th>Tanggal Soal</th><th>Aksi</th><th>Lihat</th></tr></thead>"; 
    
    $tampildiv = mysql_query("SELECT * FROM tbl_division  WHERE id_division='$_POST[id_division]'");
    $tdiv=mysql_fetch_array($tampildiv);
    
    if($tdiv[division] == "ALL")
    {
      $tampil=mysql_query("SELECT * FROM tbl_soal INNER JOIN tbl_division ON tbl_soal.id_division=tbl_division.id_division WHERE soal LIKE '%$_POST[cari]%' OR tanggal LIKE '%$_POST[cari]%'");
    }else{
      $tampil=mysql_query("SELECT * FROM tbl_soal INNER JOIN tbl_division ON tbl_soal.id_division=tbl_division.id_division WHERE (soal LIKE '%$_POST[cari]%' OR tanggal LIKE '%$_POST[cari]%') AND tbl_division.id_division = '$_POST[id_division]'");
    }

    $no=1;
    while ($r=mysql_fetch_array($tampil)){
       echo "<tr><td align='center'>$no</td>
             <td>$r[soal]</td>
             <td align='center'>$r[division]</td>
             <td align='center'>$r[tanggal]</td>
             <td align='center'>
        <a class='btn btn-outline-primary' href=?module=soal&act=editsoal&id=$r[id_soal] role='button'><i class='fa fa-edit mr-1'></i></a>
        <a class='btn btn-outline-danger' href=$aksi?module=soal&act=hapus&id=$r[id_soal] role='button'><i class='fa fa-trash mr-1'></i></a></td>";
        if ($r[aktif]=="Y") {
          echo"<td align='center'><input class='btn btn-outline-dark' type=button value='Non Aktifkan' onclick=\"window.location.href='$aksi?module=soal&act=nonaktif&id=$r[id_soal]';\"></td>";

        }else {
          echo"<td align='center'><input class='btn btn-success' type=button value='Aktifkan' onclick=\"window.location.href='$aksi?module=soal&act=aktif&id=$r[id_soal]';\"></td>";
        }
        
        echo"   </td>
    <td align='center'><a class='btn btn-outline-info' href=?module=soal&act=viewsoal&id=$r[id_soal] role='button'><i class='fa fa-eye mr-1'></i></a></td>

    </tr>";
      $no++;
    }
    echo "</table>";
    break;

  
  }
}
?>

                        </div>
                    </div>
                    </div>    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->


<script type="text/javascript">
  var $ = jQuery;
  $('#knc_jawaban').val('<?php echo $r['knc_jawaban'];?>');
</script> 
    </div>
</div>
        