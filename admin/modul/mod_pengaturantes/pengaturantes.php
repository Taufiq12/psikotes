<div class="row" id="body-row">
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
        <ul class="list-group">
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small style="color: white;">MENU</small>
            </li>
            <a href="?module=home" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-home fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Beranda</span>
                </div>
            </a>
            <a href="?module=soal" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tasks fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Kelola Soal Tes</span>
                </div>
            </a>
            <a href="?module=hasiltes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-file-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Hasil Tes</span>
                </div>
            </a>
            <a href="?module=pengaturantes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tools fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Pengaturan Tes</span>
                </div>
            </a>
            <a href="?module=users" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-users fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Daftar Peserta</span>
                </div>
            </a>   
            <a href="href=../../logout.php" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Keluar</span>
                </div>
            </a>     
        </ul>
    </div> <!-- End Sidebar -->

    <!-- MAIN -->
    <div class="col">
        
    <div id="page-wrapper">
            <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-lg-12">
                      <!--   <h3 class="page-header"> Peraturan </h3> -->

                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                    <!-- <div class="card border-danger"> -->
                        <div class="card-header bg-danger text-white">
                          Pengaturan Tes
                        </div>
                        <div class="card-body">


                          <?php
$aksi="modul/mod_pengaturantes/aksi_pengaturantes.php";
switch($_GET[act]){
  // Tampil Menuatas
  default:
    $sql  = mysql_query("SELECT * FROM tbl_pengaturan_tes");
    $r    = mysql_fetch_array($sql);


    $sqldiv ="select * from tbl_division" ;// sql to get values from mysql
    $resdiv = mysql_query($sqldiv);
    $value = $r['id_division'];
    
    echo "
          <form method=POST enctype='multipart/form-data' action=$aksi?module=pengaturantes&act=update class='form-horizontal'>
          <input type=hidden name=id value=$r[id]>";
    
    echo "<div class='form-group'>
    <label for='inputEmail3' class='col-sm-2 control-label'>Division</label>
    <div class='col-sm-4'>
      <select name='id_division' id='id_division' class='form-control'>";

    while($row=mysql_fetch_array($resdiv))
    {
      $selected=($row['id_division']==$value)? "selected" : "";
      echo '<option '.$selected.' value="'.$row['id_division'].'" >'.$row['division'].'</option>';
    }

    echo "</select>";

    echo"
      </div>
    </div>

    <div class='form-group'>
            <label for='inputEmail3' class='col-sm-2 control-label'>Nama Tes</label>
            <div class='col-lg-6'>
              <input type=text size=30 class='form-control' name=nama_tes value='$r[nama_tes]'>
            </div>
          </div>

          <div class='form-group'>
            <label for='inputEmail3' class='col-sm-3 control-label'>Tanggal Pengerjaan</label>
            <div class='col-lg-6'>
              <input type=date size=30 class='form-control' name=tgl_pengerjaan value='$r[tgl_pengerjaan]'>
            </div>
          </div>

          <div class='form-group'>
            <label for='inputEmail3' class='col-sm-3 control-label'>Waktu Pengerjaan</label>
            <div class='col-lg-6'>
              <input type=number size=30 class='form-control' name=waktu value='$r[waktu]'>
            </div>
          </div>

          <div class='form-group'>
            <label for='inputEmail3' class='col-sm-3 control-label'>Waktu Mulai</label>
            <div class='col-lg-6'>
              <input type='time' size=30 class='form-control' name=start_time value='" . date("G:i", strtotime($r['start_time'])) . "' />
            </div>
          </div>

          <div class='form-group'>
            <label for='inputEmail3' class='col-sm-3 control-label'>Waktu Selsai</label>
            <div class='col-lg-6'>
              <input type='time' size=30 class='form-control' name=end_time value='" . date("G:i", strtotime($r['end_time'])) . "' />
            </div>
          </div>

          <div class='form-group'>
            <label for='inputEmail3' class='col-sm-3 control-label'>Nilai Baik</label>
            <div class='col-lg-6'>
              <input type=number size=30 pattern=[0-9] class='form-control' name=nilai_max value='$r[nilai_max]'>
            </div>
          </div>

          <div class='form-group'>
            <label for='inputEmail3' class='col-sm-3 control-label'>Nilai Cukup</label>
            <div class='col-lg-6'>
              <input type=number size=30 pattern=[0-9] class='form-control' name=nilai_avg value='$r[nilai_avg]'>
            </div>
          </div>

          <div class='form-group'>
            <label for='inputEmail3' class='col-sm-3 control-label'>Nilai Kurang</label>
            <div class='col-lg-6'>
              <input type=number size=30 pattern=[0-9] class='form-control' name=nilai_min value='$r[nilai_min]'>
            </div>
          </div>

          <div class='form-group'>
            <label for='inputEmail3' class='col-sm-3 control-label'>Jumlah Soal</label>
            <div class='col-lg-6'>
              <input type=number size=30 pattern=[0-9] class='form-control' name=jml_soal value='$r[jml_soal]'>
            </div>
          </div>

          <div class='form-group'>
            <label for='inputEmail3' class='col-sm-2 control-label'>Peraturan</label>
            <div class='col-lg-12'>
              <textarea name='peraturan' style='width: 100%; height: 100%;'>$r[peraturan]</textarea>
            </div>
          </div>

          <div class='form-group'>
            <label for='inputEmail3' class='col-sm-2 control-label'></label>
            <div class='col-sm-5'>
              <input type='submit' value='Perbarui' class='btn btn-primary'>
            </div>
          </div>

          

         </form>";
    break;  
  
}
?>

                        </div>
                        
                    </div>
                    </div>    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->




    </div>
</div>
        