<div class="row" id="body-row">
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
        <ul class="list-group">
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small style="color: white;">MENU</small>
            </li>
            <a href="?module=home" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-home fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Beranda</span>
                </div>
            </a>
            <a href="?module=soal" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tasks fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Kelola Soal Tes</span>
                </div>
            </a>
            <a href="?module=hasiltes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-file-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Hasil Tes</span>
                </div>
            </a>
            <a href="?module=pengaturantes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tools fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Pengaturan Tes</span>
                </div>
            </a>
            <a href="?module=users" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-users fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Daftar Peserta</span>
                </div>
            </a>      
            <a href="../../logout.php" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Keluar</span>
                </div>
            </a>     
        </ul>
    </div> <!-- End Sidebar -->

    <!-- MAIN -->
    <div class="col">
        
    <div id="page-wrapper">
            <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-lg-12">
                      <!--   <h3 class="page-header"> Peraturan </h3> -->

                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                    
                        <div class="card-header bg-danger text-white">
                          Hasil Tes
                        </div>
                        <div class="card-body">
                          <?php
$aksi="modul/mod_hasiltes/belummengerjakan.php";
switch($_GET[act]){
  // Tampil Hasil tes Users
  default:
    $sql  = mysql_query("SELECT * FROM tbl_pengaturan_tes");
    $r    = mysql_fetch_array($sql);
    
    $tampildiv = mysql_query("SELECT * FROM tbl_division  WHERE id_division=$r[id_division]");
    $tdiv=mysql_fetch_array($tampildiv);

    if($tdiv[division] == "ALL")
    {
        session_start();
        $id_paging = $_SESSION['id_paging'];

        // Langkah 1. Tentukan batas,cek halaman & posisi data
        $batas   = 10;
        $halaman = $id_paging;

        if(empty($halaman)){
        $posisi  = 0;
        $halaman = 1;
        }
        else{ 
        $posisi  = ($halaman-1) * $batas; 
        }
        

        // Langkah 2. Sesuaikan query dengan posisi dan batas
        $tampil = mysql_query("SELECT *
                            FROM tbl_user a
                            INNER JOIN (
                            SELECT DISTINCT id_division 
                            FROM tbl_soal WHERE aktif='Y' AND  Tanggal = '$r[tgl_pengerjaan]') b ON a.id_division = b.id_division
                            INNER JOIN tbl_division c ON c.id_division = a.id_division
                            WHERE id_user NOT IN (
                            SELECT id_user
                            FROM tbl_nilai
                            WHERE Tanggal = '$r[tgl_pengerjaan]')
                            ORDER BY a.nama DESC");



        $no = $posisi+1;

        // Langkah 3: Hitung total data dan halaman serta link 1,2,3 
        $query2 = mysql_query("SELECT *
                            FROM tbl_user a
                            INNER JOIN (
                            SELECT DISTINCT id_division 
                            FROM tbl_soal WHERE aktif='Y' AND  Tanggal = '$r[tgl_pengerjaan]') b ON a.id_division = b.id_division
                            INNER JOIN tbl_division c ON c.id_division = a.id_division
                            WHERE id_user NOT IN (
                            SELECT id_user
                            FROM tbl_nilai
                            WHERE Tanggal = '$r[tgl_pengerjaan]')
                            ORDER BY a.nama DESC");

        $jmldata    = mysql_num_rows($query2);
        $jmlhalaman = ceil($jmldata/$batas);

        $datasoalmin = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND  id_soal= $min[id_soal]");
        $soalmin = mysql_fetch_array($datasoalmin);
    }else{
        session_start();
        $id_paging = $_SESSION['id_paging'];

        // Langkah 1. Tentukan batas,cek halaman & posisi data
        $batas   = 10;
        $halaman = $id_paging;

        if(empty($halaman)){
        $posisi  = 0;
        $halaman = 1;
        }
        else{ 
        $posisi  = ($halaman-1) * $batas; 
        }
        

        // Langkah 2. Sesuaikan query dengan posisi dan batas
        $tampil = mysql_query("SELECT *
                            FROM tbl_user a
                            INNER JOIN (
                            SELECT DISTINCT id_division 
                            FROM tbl_soal WHERE aktif='Y' AND  Tanggal = '$r[tgl_pengerjaan]') b ON a.id_division = b.id_division
                            INNER JOIN tbl_division c ON c.id_division = a.id_division
                            WHERE id_user NOT IN (
                            SELECT id_user
                            FROM tbl_nilai
                            WHERE Tanggal = '$r[tgl_pengerjaan]') AND a.id_division = $r[id_division]
                            ORDER BY a.nama DESC");



        $no = $posisi+1;

        // Langkah 3: Hitung total data dan halaman serta link 1,2,3 
        $query2 = mysql_query("SELECT *
                            FROM tbl_user a
                            INNER JOIN (
                            SELECT DISTINCT id_division 
                            FROM tbl_soal WHERE aktif='Y' AND  Tanggal = '$r[tgl_pengerjaan]') b ON a.id_division = b.id_division
                            INNER JOIN tbl_division c ON c.id_division = a.id_division
                            WHERE id_user NOT IN (
                            SELECT id_user
                            FROM tbl_nilai
                            WHERE Tanggal = '$r[tgl_pengerjaan]') AND a.id_division = $r[id_division]
                            ORDER BY a.nama DESC");

        $jmldata    = mysql_num_rows($query2);
        $jmlhalaman = ceil($jmldata/$batas);

        $datasoalmin = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND  id_soal= $min[id_soal] AND id_division = $r[id_division]");
        $soalmin = mysql_fetch_array($datasoalmin);
    }

    include "config/koneksi.php";
    $sql ="select * from tbl_division" ;// sql to get values from mysql
    $res = mysql_query($sql);
    $value = $r['id_division'];

    echo "<div class='row'>
    <div class='col-lg-6'>
        <a class='btn btn-warning' href='cetak/cetakbelummengerjakan' role='button' target='_blank' rel='noopener noreferrer'><span class='fa fa-print fa-fw mr-3'></span>Cetak</a>  
        <a class='btn btn-warning' href='cetak/cetakbelummengerjakan_excel' role='button'><i class='fa fa-file-excel fa-fw mr-3'></i>Export ke Excel</a>
        </div>
    </div>
    </br>";

      echo "
      <div col-lg-6>
            <form class='form-inline'method='POST' action=?module=belummengerjakan&act=carihasil>
                <div class='form-group mb-2'>
                    <input class='form-control' type=date name='tanggal'  placeholder='Masukkan Tanggal' list='auto'/>
                    <select name='id_division' id='id_division' class='form-control'>";
                        while($row=mysql_fetch_array($res))
                        {
                        $selected=($row['id_division']==$value)? "selected" : "";
                        echo '<option '.$selected.' value="'.$row['id_division'].'" >'.$row['division'].'</option>';
                        }
              echo "</select>
                    <button class='btn btn-success ml-3' type='submit'>
                        <i class='fa fa-search mr-1'></i>Cari
                    </button>
                </div>
      </div>
      <table class='table table-hover mt-3'>
      <thead><tr><th>No</th><th>Nama</th><th>Alamat</th><th>Division</th></tr></thead>";

    while ($r=mysql_fetch_array($tampil)){
    $tgl = tgl_indo($r[tanggal]);

       echo "<tr><td>$no</td>
            <td>$r[nama]</td>
            <td>$r[alamat]</td>
            <td align='center'>$r[division]</td>
   </td>
      </tr>";
      $no++;

    }
    echo "</table>";
    break;

    //Search
    case "carihasil":
        session_start();
        $_SESSION[tanggal]= $_POST[tanggal];
        $_SESSION[id_division]= $_POST[id_division];

        $tampildiv = mysql_query("SELECT * FROM tbl_division  WHERE id_division='$_POST[id_division]'");
        $tdiv=mysql_fetch_array($tampildiv);

        if($tdiv[division] == "ALL")
        {
            $tampil = mysql_query("SELECT nama,jk,email,benar,salah,kosong,score,keterangan,tanggal,division FROM tbl_user INNER JOIN tbl_nilai ON tbl_user.id_user=tbl_nilai.id_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE tbl_nilai.tanggal='$_POST[tanggal]' ORDER BY tbl_nilai.tanggal,tbl_nilai.benar DESC");
        
            $tampil = mysql_query("SELECT *
                            FROM tbl_user a
                            INNER JOIN (
                            SELECT DISTINCT id_division 
                            FROM tbl_soal WHERE aktif='Y') b ON a.id_division = b.id_division
                            INNER JOIN tbl_division c ON c.id_division = a.id_division
                            WHERE id_user NOT IN (
                            SELECT id_user
                            FROM tbl_nilai
                            WHERE Tanggal = '$_POST[tanggal]')
                            ORDER BY a.nama DESC");

            $maks = mysql_fetch_array($datamaks);

            $datasoalmaks = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND  id_soal= $maks[id_soal]");
            $soalmaks = mysql_fetch_array($datasoalmaks);
        }else{
            $tampil = mysql_query("SELECT nama,jk,email,benar,salah,kosong,score,keterangan,tanggal,division FROM tbl_user INNER JOIN tbl_nilai ON tbl_user.id_user=tbl_nilai.id_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE tbl_nilai.tanggal='$_POST[tanggal]' AND tbl_division.id_division = '$_POST[id_division]' ORDER BY tbl_nilai.tanggal,tbl_nilai.benar DESC");
        
            $tampil = mysql_query("SELECT *
                            FROM tbl_user a
                            INNER JOIN (
                            SELECT DISTINCT id_division 
                            FROM tbl_soal WHERE aktif='Y') b ON a.id_division = b.id_division
                            INNER JOIN tbl_division c ON c.id_division = a.id_division
                            WHERE id_user NOT IN (
                            SELECT id_user
                            FROM tbl_nilai
                            WHERE Tanggal = '$_POST[tanggal]') AND c.id_division = '$_POST[id_division]'
                            ORDER BY a.nama DESC");

            $maks = mysql_fetch_array($datamaks);

            $datasoalmaks = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND  id_soal= $maks[id_soal] AND id_division='$_POST[id_division]'");
            $soalmaks = mysql_fetch_array($datasoalmaks);
        }

        echo "<div class='row'>
          <div class='col-lg-6'>
              <a class='btn btn-warning' href='cetak/cetakbelummengerjakantanggal' role='button' target='_blank' rel='noopener noreferrer'><span class='fa fa-print fa-fw mr-3'></span>Cetak</a>
              <a class='btn btn-warning' href='cetak/cetakbelummengerjakantanggal_excel' role='button'><i class='fa fa-file-excel fa-fw mr-3'></i>Export ke Excel</a>
              <a class='btn btn-success' href='?module=belummengerjakan' role='button'><i class='fa fa-user-check fa-fw mr-3'></i>Kembali</a>     
              </div>
          </div>
            <table class='table table-hover mt-3'>
            <thead><tr><th>No</th><th>Nama</th><th>Alamat</th><th>Division</th></tr></thead>";
        $no=1;
        while ($r=mysql_fetch_array($tampil)){
        $tgl = tgl_indo($r[tanggal]);
    
        echo "<tr><td>$no</td>
            <td>$r[nama]</td>
            <td>$r[alamat]</td>
            <td align='center'>$r[division]</td>
            </td>
                </tr>";
          $no++;
        }
        echo "</table>";
        break;
}
?>



                        </div>
                    </div>
                    </div>    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->



    </div>
</div>