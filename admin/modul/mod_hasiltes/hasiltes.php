<div class="row" id="body-row">
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
        <ul class="list-group">
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small style="color: white;">MENU</small>
            </li>
            <a href="?module=home" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-home fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Beranda</span>
                </div>
            </a>
            <a href="?module=soal" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tasks fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Kelola Soal Tes</span>
                </div>
            </a>
            <a href="?module=hasiltes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-file-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Hasil Tes</span>
                </div>
            </a>
            <a href="?module=pengaturantes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tools fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Pengaturan Tes</span>
                </div>
            </a>
            <a href="?module=users" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-users fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Daftar Peserta</span>
                </div>
            </a>      
            <a href="../../logout.php" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Keluar</span>
                </div>
            </a>     
        </ul>
    </div> <!-- End Sidebar -->

    <!-- MAIN -->
    <div class="col">
        
    <div id="page-wrapper">
            <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-lg-12">
                      <!--   <h3 class="page-header"> Peraturan </h3> -->

                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                    
                        <div class="card-header bg-danger text-white">
                          Hasil Tes
                        </div>
                        <div class="card-body">
                          <?php
$aksi="modul/mod_hasiltes/aksi_hasiltes.php";
switch($_GET[act]){
  // Tampil Hasil tes Users
  default:

    session_start();
    $id_paging = $_SESSION['id_paging'];

    // Langkah 1. Tentukan batas,cek halaman & posisi data
    $batas   = 10;
    $halaman = $id_paging;

    if(empty($halaman)){
      $posisi  = 0;
      $halaman = 1;
    }
    else{ 
      $posisi  = ($halaman-1) * $batas; 
    }

    // Langkah 2. Sesuaikan query dengan posisi dan batas
    $tampil = mysql_query("SELECT * FROM tbl_nilai INNER JOIN tbl_user ON tbl_nilai.id_user=tbl_user.id_user 
                           INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division 
                           WHERE tbl_nilai.tanggal = $tgl_sekarang ORDER BY tbl_nilai.tanggal,tbl_nilai.score  DESC");

    $no = $posisi+1;

    // Langkah 3: Hitung total data dan halaman serta link 1,2,3 
    $query2     = mysql_query("SELECT * FROM tbl_nilai INNER JOIN tbl_user ON tbl_nilai.id_user=tbl_user.id_user 
                               INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE tbl_nilai.tanggal = $tgl_sekarang ORDER BY tbl_nilai.tanggal,tbl_nilai.score DESC");
    $jmldata    = mysql_num_rows($query2);
    $jmlhalaman = ceil($jmldata/$batas);

    // Nilai Tinggi
    $datamaks = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
    FROM tbl_salah_jawab
    JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
    WHERE tbl_soal.aktif='Y' AND  tbl_salah_jawab.tanggal = $tgl_sekarang 
    GROUP BY id_soal
    ORDER BY count(tbl_soal.id_soal) DESC
    LIMIT 1");

    $maks = mysql_fetch_array($datamaks);

    $datasoalmaks = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND  id_soal= $maks[id_soal]");
    $soalmaks = mysql_fetch_array($datasoalmaks);

    // Nilai Rata-Rata
    $datarata = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
                            FROM tbl_salah_jawab
                            JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
                            WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = $tgl_sekarang 
                            GROUP BY id_soal
                            ORDER BY count(tbl_soal.id_soal) ASC
                            LIMIT 1,2");
    $rata = mysql_fetch_array($datarata);

    // Nilai Rendah
    $datamin = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
        FROM tbl_salah_jawab
        JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
        WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = $tgl_sekarang 
        GROUP BY id_soal
        ORDER BY count(tbl_soal.id_soal) ASC
        LIMIT 1");
    $min = mysql_fetch_array($datamin);

    $datasoalmin = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND  id_soal= $min[id_soal]");
    $soalmin = mysql_fetch_array($datasoalmin);

    //Update score dan keterangan
    mysql_query("UPDATE tbl_nilai
    INNER JOIN tbl_pengaturan_tes ON tbl_pengaturan_tes.id = 1
    SET tbl_nilai.keterangan = 
            CASE 
            WHEN tbl_nilai.score > tbl_pengaturan_tes.nilai_avg THEN 'Baik' 
            WHEN tbl_nilai.score > tbl_pengaturan_tes.nilai_min and score < tbl_pengaturan_tes.nilai_max THEN 'Cukup' 
            WHEN tbl_nilai.score < tbl_pengaturan_tes.nilai_min THEN 'Kurang' END ,
            score = (100/tbl_pengaturan_tes.jml_soal)*benar 
    WHERE tbl_nilai.tanggal=$tgl_sekarang");

    //delete history
    mysql_query("DELETE FROM tbl_nilai WHERE tanggal <= DATE(DATE_SUB(now(), INTERVAL 3 MONTH))");
    mysql_query("DELETE FROM tbl_salah_jawab WHERE tanggal <= DATE(DATE_SUB(now(), INTERVAL 3 MONTH))");
    mysql_query("DELETE FROM tbl_log_jawab WHERE tanggal <= DATE(DATE_SUB(now(), INTERVAL 3 MONTH))");

    include "config/koneksi.php";
    $sql ="select * from tbl_division" ;// sql to get values from mysql
    $res = mysql_query($sql);
    $value = $r['id_division'];

    echo "<div class='row'>
    <div class='col-lg-6'>
        <a class='btn btn-warning' href='cetak/cetakhasiltes' role='button' target='_blank' rel='noopener noreferrer'><span class='fa fa-print fa-fw mr-3'></span>Cetak</a> 
        <a class='btn btn-warning' href='cetak/cetakhasiltes_excel' role='button'><i class='fa fa-file-excel fa-fw mr-3'></i>Export ke Excel</a>
        </div>
    </div>
    </br>";

      echo "
      <div col-lg-6>
            <form class='form-inline'method='POST' action=?module=hasiltes&act=carihasil>
                <div class='form-group mb-2'>
                    <input class='form-control' type=date name='tanggal'  placeholder='Masukkan Tanggal' list='auto'  required/>
                    <select name='id_division' id='id_division' class='form-control'>";
                        while($row=mysql_fetch_array($res))
                        {
                        $selected=($row['id_division']==$value)? "selected" : "";
                        echo '<option '.$selected.' value="'.$row['id_division'].'" >'.$row['division'].'</option>';
                        }
              echo "</select>
                    <button class='btn btn-success ml-3' type='submit'>
                        <i class='fa fa-search mr-1'></i>Cari
                    </button>
                </div>
      </div>
      <table class='table table-hover mt-3'>
      <thead><tr align='center'><th>No</th><th>Nama</th><th>Division</th><th>Benar</th><th>Salah</th><th>Kosong</th><th>Nilai</th><th>Keterangan</th><th>Lihat</th><th>Hapus</th></tr></thead>";

    while ($r=mysql_fetch_array($tampil)){
    $tgl = tgl_indo($r[tanggal]);

       echo "<tr><td>$no</td>
            <td>$r[nama]</td>
            <td align='center'>$r[division]</td>
            <td align='center'>$r[benar]</td>
        <td align='center'>$r[salah]</td>
        <td align='center'>$r[kosong]</td>
        <td align='center'>$r[score]</td>
        <td align='center'>$r[keterangan]</td>
        <td align='center'><a class='btn btn-outline-info' href='?module=hasiltes&act=lihat&id=$r[id_user]&tanggal=$tgl_sekarang' role='button'><i class='fa fa-eye mr-1'></i>Lihat</a></td>
        <td align='center'><a class='btn btn-outline-info' href='?module=hasiltes&act=hapus&id=$r[id_user]&tanggal=$tgl_sekarang' role='button'><i class='fa fa-trash mr-1'></i>Hapus</a></td>
   </td>
      </tr>";
      $no++;

    }
    echo "</table>";
    echo "<table class='table table-hover mt-3'>";
    echo "<tr>
            <td>Terbanyak Salah</td>
            <td>$soalmaks[soal]</td>
            </tr>";
    echo "<tr>
            <td>Rata-Rata Salah</td>
            <td>$rata[soal]</td>
          </tr>";
    echo "<tr>
            <td>Terkecil Salah</td>
            <td>$soalmin[soal]</td>
            </tr>";
    echo "</table>";
    break;

    //Search
    case "carihasil":
        session_start();
        $_SESSION[tanggal]= $_POST[tanggal];
        $_SESSION[id_division]= $_POST[id_division];
        
        $tampildiv = mysql_query("SELECT * FROM tbl_division  WHERE id_division='$_POST[id_division]'");
        $tdiv=mysql_fetch_array($tampildiv);

        if($tdiv[division] == "ALL")
        {
            $tampil = mysql_query("SELECT tbl_user.id_user,nama,jk,email,benar,salah,kosong,score,keterangan,tanggal,division FROM tbl_user INNER JOIN tbl_nilai ON tbl_user.id_user=tbl_nilai.id_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE tbl_nilai.tanggal='$_POST[tanggal]' ORDER BY tbl_nilai.tanggal,tbl_nilai.score DESC");
            
            $datamaks = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
            FROM tbl_salah_jawab
            JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
            WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = '$_POST[tanggal]'
            GROUP BY id_soal
            ORDER BY count(tbl_soal.id_soal) DESC
            LIMIT 1");

            // Nilai Tinggi
            $maks = mysql_fetch_array($datamaks);

            $datasoalmaks = mysql_query("SELECT * FROM tbl_soal WHERE id_soal= $maks[id_soal]");
            $soalmaks = mysql_fetch_array($datasoalmaks);

            // Nilai Rata-Rata
            $datarata = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
                                    FROM tbl_salah_jawab
                                    JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
                                    WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = '$_POST[tanggal]' 
                                    GROUP BY id_soal
                                    ORDER BY count(tbl_soal.id_soal) ASC
                                    LIMIT 1,2");
            $rata = mysql_fetch_array($datarata);

            // Nilai Rendah
            $datamin = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
                FROM tbl_salah_jawab
                JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
                WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = '$_POST[tanggal]' 
                GROUP BY id_soal
                ORDER BY count(tbl_soal.id_soal) ASC
                LIMIT 1");
            $min = mysql_fetch_array($datamin);

            $datasoalmin = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND id_soal= $min[id_soal]");
            $soalmin = mysql_fetch_array($datasoalmin);

        }else{
            $tampil = mysql_query("SELECT tbl_user.id_user,nama,jk,email,benar,salah,kosong,score,keterangan,tanggal,division FROM tbl_user INNER JOIN tbl_nilai ON tbl_user.id_user=tbl_nilai.id_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE tbl_nilai.tanggal='$_POST[tanggal]' AND tbl_division.id_division = '$_POST[id_division]' ORDER BY tbl_nilai.tanggal,tbl_nilai.score DESC");
        
            $datamaks = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
            FROM tbl_salah_jawab
            JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
            join tbl_division on tbl_division.id_division = tbl_soal.id_division
            WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = '$_POST[tanggal]' AND tbl_division.id_division = '$_POST[id_division]'
            GROUP BY id_soal
            ORDER BY count(tbl_soal.id_soal) DESC
            LIMIT 1");

            // Nilai Tinggi
            $maks = mysql_fetch_array($datamaks);

            $datasoalmaks = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND id_soal= $maks[id_soal]");
            $soalmaks = mysql_fetch_array($datasoalmaks);

            // Nilai Rata-Rata
            $datarata = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
                                    FROM tbl_salah_jawab
                                    JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
                                    join tbl_division on tbl_division.id_division = tbl_soal.id_division
                                    WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = '$_POST[tanggal]' AND tbl_division.id_division = '$_POST[id_division]'
                                    GROUP BY id_soal
                                    ORDER BY count(tbl_soal.id_soal) ASC
                                    LIMIT 1,2");
            $rata = mysql_fetch_array($datarata);

            // Nilai Rendah
            $datamin = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
                FROM tbl_salah_jawab
                JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
                join tbl_division on tbl_division.id_division = tbl_soal.id_division
                WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = '$_POST[tanggal]' AND tbl_division.id_division = '$_POST[id_division]'
                GROUP BY id_soal
                ORDER BY count(tbl_soal.id_soal) ASC
                LIMIT 1");
            $min = mysql_fetch_array($datamin);

            $datasoalmin = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND id_soal= $min[id_soal] AND id_division = '$_POST[id_division]'");
            $soalmin = mysql_fetch_array($datasoalmin);
        } 
        
        echo "<div class='row'>
          <div class='col-lg-6'>
              <a class='btn btn-warning' href='cetak/cetaktanggal' role='button' target='_blank' rel='noopener noreferrer'><span class='fa fa-print fa-fw mr-3'></span>Cetak</a>
              <a class='btn btn-warning' href='cetak/cetaktanggal_excel' role='button'><i class='fa fa-file-excel fa-fw mr-3'></i>Export ke Excel</a>
              <a class='btn btn-success' href='?module=hasiltes' role='button'><i class='fa fa-user-check fa-fw mr-3'></i>Kembali</a>     
              </div>
          </div>
            <table class='table table-hover mt-3'>
              <thead><tr align='center'><th>No</th><th>Nama</th><th>Division</th><th>Benar</th><th>Salah</th><th>Kosong</th><th>Nilai</th><th>Keterangan</th><th>Lihat</th></tr></thead>"; 
        $no=1;
        while ($r=mysql_fetch_array($tampil)){
        $tgl = tgl_indo($r[tanggal]);
    
           echo "<tr><td>$no</td>
                 <td>$r[nama]</td>
                 <td align='center'>$r[division]</td>
                <td align='center'>$r[benar]</td>
            <td align='center'>$r[salah]</td>
            <td align='center'>$r[kosong]</td>
            <td align='center'>$r[score]</td>
            <td align='center'>$r[keterangan]</td>
            <td align='center'><a class='btn btn-outline-info' href='?module=hasiltes&act=lihat&id=$r[id_user]&tanggal=$_POST[tanggal]' role='button'><i class='fa fa-eye mr-1'></i>Lihat</a></td>
          </tr>";
          $no++;
        }
        echo "</table>";
        echo "<table class='table table-hover mt-3'>";
        echo "<tr>
                <td>Terbanyak Salah</td>
                <td>$soalmaks[soal]</td>
                </tr>";
        echo "<tr>
                <td>Rata-Rata Salah</td>
                <td>$rata[soal]</td>
              </tr>";
        echo "<tr>
                <td>Terkecil Salah</td>
                <td>$soalmin[soal]</td>
                </tr>";
        echo "</table>";
        break;


        //lihat
    case "lihat":
        session_start();
        $_SESSION[tanggal]= $_POST[tanggal];
        $tampil = mysql_query("SELECT b.nama,c.soal,a.jawaban_benar,a.jawaban_salah, a.tanggal
                                FROM tbl_log_jawab a
                                JOIN tbl_user b on a.id_user = b.id_user
                                JOIN tbl_soal c on c.id_soal = a.id_soal
                                WHERE c.aktif='Y' AND a.id_user='$_GET[id]' and a.tanggal='$_GET[tanggal]'");

        $data = mysql_query("SELECT * FROM tbl_user WHERE id_user ='$_GET[id]'");
        $query = mysql_fetch_array($data);
       
        echo "<div class='row'>
          <div class='col-lg-6'>
              <a class='btn btn-success' href='?module=hasiltes' role='button'><i class='fa fa-user-check fa-fw mr-3'></i>Kembali</a>     
          </div>
          </div>
          <div>
            <br> Nama :  $query[nama]
          </div>
            <table class='table table-hover mt-3'>
           
              <thead><tr align='center'><th>No</th><th>Soal</th><th>Jawaban Benar</th><th>Jawaban Salah</th><th>Tanggal</th></tr></thead>"; 
        $no=1;
        while ($r=mysql_fetch_array($tampil)){  
           echo "<tr><td>$no</td>
                    <td>$r[soal]</td>
                    <td align='center'>$r[jawaban_benar]</td>
                    <td align='center'>$r[jawaban_salah]</td>
                    <td align='center'>$r[tanggal]</td>
                </tr>";
          $no++;
        }
        echo "</table>";
        break;

    
        case "hapus":
            session_start();

            //Cek tanggal untuk soal
            $qrytglsoal=mysql_query("SELECT tgl_pengerjaan FROM tbl_pengaturan_tes");
            $qtglsoal=mysql_fetch_array($qrytglsoal);
            
            mysql_query("DELETE FROM tbl_nilai WHERE id_user='$_GET[id]' and tanggal='$qtglsoal[tgl_pengerjaan]'");
            mysql_query("DELETE FROM tbl_log_jawab WHERE id_user='$_GET[id]' and tanggal='$qtglsoal[tgl_pengerjaan]'");
            mysql_query("DELETE FROM tbl_salah_jawab WHERE id_user='$_GET[id]' and tanggal='$qtglsoal[tgl_pengerjaan]'");

            header("Location: ?module=hasiltes");
            
            break;

}
?>



                        </div>
                    </div>
                    </div>    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->



    </div>
</div>