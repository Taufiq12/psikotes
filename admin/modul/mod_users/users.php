<div class="row" id="body-row">
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
        <ul class="list-group">
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small style="color: white;">MENU</small>
            </li>
            <a href="?module=home" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-home fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Beranda</span>
                </div>
            </a>
            <a href="?module=soal" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tasks fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Kelola Soal Tes</span>
                </div>
            </a>
            <a href="?module=hasiltes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-file-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Hasil Tes</span>
                </div>
            </a>
            <a href="?module=pengaturantes" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tools fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Pengaturan Tes</span>
                </div>
            </a>
            <a href="?module=users" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-users fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Daftar Peserta</span>
                </div>
            </a>
            <a href="href=../../logout.php" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed" style="color: white;">Keluar</span>
                </div>
            </a>        
        </ul>
    </div> <!-- End Sidebar -->

    <!-- MAIN -->
    <div class="col">
        
    <div id="page-wrapper">
            <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-lg-12">
                      <!--   <h3 class="page-header"> Peraturan </h3> -->

                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                    
                        <div class="card-header bg-danger text-white">
                            Daftar Peserta
                        </div>
                        <div class="card-body">


                          
<?php
$aksi="modul/mod_users/aksi_users.php";
switch($_GET[act]){
  // Tampil User
  default:
//Template Soal
echo "<div class='row'><div class='col-lg-6'>
<a href='../DownloadTemplate/formatuser.xlsx' class='btn btn-primary'> <span class='glyphicon glyphicon-download'></span>Download Format</a></div></div></br></br>";

//File Upload
echo "<div class='row'>
<div class='col-lg-6'>
  <form method='post' enctype='multipart/form-data' action='../uploaduser.php'>
    Silakan Pilih File Excel: 
    <input type='file' name='file' id='file' accept='.xls,.xlsx'>
    <button type='submit' id='submit' name='import' class='btn btn-primary'>Import</button>
  </form>
</div>
</div>
</br>
</br>";

include "config/koneksi.php";
$sql ="select * from tbl_division" ;// sql to get values from mysql
$res = mysql_query($sql);
$value = $r['id_division'];

// cetak
echo "<div class='row'>
<div class='col-lg-6'>
    <a class='btn btn-warning' href='cetak/cetakpeserta' role='button' target='_blank' rel='noopener noreferrer'><span class='fa fa-print fa-fw mr-3'></span>Cetak</a>
    <a class='btn btn-warning' href='cetak/cetakpeserta_excel' role='button'><i class='fa fa-file-excel fa-fw mr-3'></i>Export ke Excel</a>
    </div>";
//   Cari
  echo "<div col-lg-6>
<form class='form-inline'method='POST' action=?module=users&act=cariusers>
<div class='form-group mx-sm-3 mb-2'>
<input class='form-control' type=text name='cari'  placeholder='Masukkan Nama' list='auto'/>
<select name='id_division' id='id_division' class='form-control'>";
    while($row=mysql_fetch_array($res))
    {
      $selected=($row['id_division']==$value)? "selected" : "";
      echo '<option '.$selected.' value="'.$row['id_division'].'" >'.$row['division'].'</option>';
    }
echo "</select>
<button class='btn btn-success ml-3' type='submit'><i class='fa fa-search mr-1'></i>Cari</button></div></div>";

echo"<datalist id='auto'>";
$qry=mysql_query("SELECT * FROM tbl_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division");
while ($t=mysql_fetch_array($qry)) {
echo "<option value='$t[nama]'>";
}
echo"</datalist></form>
</div>";

        $sql  = mysql_query("SELECT * FROM tbl_pengaturan_tes");
        $r    = mysql_fetch_array($sql);

        $tampildiv = mysql_query("SELECT * FROM tbl_division  WHERE id_division=$r[id_division]");
        $tdiv=mysql_fetch_array($tampildiv);

        if($tdiv[division] == "ALL")
        {
          session_start();
          $id_paging = $_SESSION['id_paging'];

          // Langkah 1. Tentukan batas,cek halaman & posisi data
          $batas   = 10;
          $halaman = $id_paging;

          if(empty($halaman)){
            $posisi  = 0;
            $halaman = 1;
          }
          else{ 
            $posisi  = ($halaman-1) * $batas; 
          }

          // Langkah 2. Sesuaikan query dengan posisi dan batas
          $tampil = mysql_query("SELECT * FROM tbl_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division LIMIT $posisi, $batas");

          $no = $posisi+1;

          // Langkah 3: Hitung total data dan halaman serta link 1,2,3 
          $query2     = mysql_query("SELECT * FROM tbl_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division");
          $jmldata    = mysql_num_rows($query2);
          $jmlhalaman = ceil($jmldata/$batas);
        }else{
          session_start();
          $id_paging = $_SESSION['id_paging'];

          // Langkah 1. Tentukan batas,cek halaman & posisi data
          $batas   = 10;
          $halaman = $id_paging;

          if(empty($halaman)){
            $posisi  = 0;
            $halaman = 1;
          }
          else{ 
            $posisi  = ($halaman-1) * $batas; 
          }

          // Langkah 2. Sesuaikan query dengan posisi dan batas
          $tampil = mysql_query("SELECT * FROM tbl_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE tbl_user.id_division=$r[id_division] LIMIT $posisi, $batas");

          $no = $posisi+1;

          // Langkah 3: Hitung total data dan halaman serta link 1,2,3 
          $query2     = mysql_query("SELECT * FROM tbl_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE tbl_user.id_division=$r[id_division]");
          $jmldata    = mysql_num_rows($query2);
          $jmlhalaman = ceil($jmldata/$batas);
        }

        echo "<br> Halaman : ";

        for($i=1;$i<=$jmlhalaman;$i++)
        if ($i != $halaman){
        echo " <a href=?module=users?halaman=$i>$i</a> | ";
        }
        else{ 
        echo " <b>$i</b> | "; 
        }
        echo "<p>Total peserta : <b>$jmldata</b> orang</p>";


      echo "
      <div class='row'>
      <div class='col-12 col sm-3'></div>
    <table class='table table-hover mt-3'>
          <tr align='center'><th>No</th><th>Nama</th><th>Jenis Kelamin</th><th>Division</th><th>Status</th><th>Lihat</th><th>Hapus</th><th>Aksi</th></tr>"; 
    // $no=1;
    // while ($r=mysql_fetch_array($tampil)){
       while($mas=mysql_fetch_array($tampil)){
       echo "<tr><td align='center'>$no</td>
             <td>$mas[username]</td>
        <td align='center'>$mas[jk]</td>
        <td align='center'>$mas[division]</td>";

        echo  "<td align=center>$mas[statusaktif]</td>
        <td align='center'><a class='btn btn-outline-info' href='?module=users&act=lihat&id=$mas[id_user]' role='button'><i class='fa fa-eye mr-1'></i>Lihat</a></td>
             <td align='center'><a class='btn btn-outline-danger' href=$aksi?module=users&act=hapus&id=$mas[id_user] role='button'><i class='fa fa-trash mr-1'></i>Hapus</a></td>";
        if ($mas[statusaktif]=="Y") {
          echo"<td align='center'><input type=button class='btn btn-outline-dark' value='Non Aktifkan' onclick=\"window.location.href='$aksi?module=users&act=nonaktif&id=$mas[id_user]';\"></td>";

        }else {
          echo"<td align='center'><input class='btn btn-outline-success' type=button value='Aktifkan' onclick=\"window.location.href='$aksi?module=users&act=aktif&id=$mas[id_user]';\"></td>";
        }
        echo"</tr>";
      $no++;
    }
    echo "</table>
    </div>";
    break;

  case "lihat":
       $tampil = mysql_query("SELECT * FROM tbl_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE id_user='$_GET[id]'");
    $t=mysql_fetch_array($tampil);
    echo "
  <form method=POST class='form-horizontal' action='$aksi?module=users&act=update' enctype='multipart/form-data'>
  <input type=hidden name=id_user value='$t[id_user]'>
  <table width='60%'>
    <tr><th colspan=2 align='center'>DETAIL PESERTA</th></tr>
    <tr><td>User Name</td><td> <input type=text name='username' class='form-control' value='$t[username]'required/></td></tr>
    <tr><td>Nama</td><td> <input type=text name='nama' class='form-control' value='$t[nama]'required/></td></tr>
    <tr><td>Jenis Kelamin </td><td><select name='jk' id='knc_jawaban' class='form-control'>
                                    <option ".($t[jk] == 'Pria' ? 'selected' : '')." value='Pria'>Pria</option>
                                    <option ".($t[jk] == 'Perempuan' ? 'selected' : '')." value='Perempuan'>Perempuan</option>
                                    </select></td> </tr>
    <tr><td>Division </td><td>$t[division]</td> </tr>
    <tr><td>Email </td><td><input type=text name='email' class='form-control' value='$t[email]' required/></td></tr>
    <tr><td>Password Encrypt</td><td>$t[password]</td></tr>
    <tr><td>New Password </td><td><input type=text name='password' class='form-control' required/></td></tr>
    <tr> <td>Telp</td><td><input type=text name='telp' class='form-control' value='$t[telp]' required/></td></tr>
    <tr><td>Alamat</td><td><input type=text name='alamat' class='form-control' value='$t[alamat]' required/></td></tr>
  </table>
  <button class='btn btn-primary' type='submit' name='submit'><i class='fa fa-save mr-1'></i>Simpan</button>
                        <input type=button value=Batal onclick=self.history.back() class='btn btn-danger'>";
  break;


  case "cariusers":
       echo"<h2><i class='fa fa-search mr-3'></i>Hasil Pencarian</h2><hr/>
       <a class='btn btn-success mt-1 mb-1' href='?module=users' role='button'><i class='fa fa-sign-out-alt mr-1'></i>Kembali</a>
     <table class='table table-hover mt-3'>
          <thead><tr align='center'><th>No</th><th>Nama</th><th>Email</th><th>Aksi</th><th>Status</th><th>Lihat</th></tr></thead>"; 

    $tampildiv = mysql_query("SELECT * FROM tbl_division  WHERE id_division='$_POST[id_division]'");
    $tdiv=mysql_fetch_array($tampildiv);

    if($tdiv[division] == "ALL")
    {
      $tampil=mysql_query("SELECT * FROM tbl_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE tbl_user.nama LIKE '%$_POST[cari]%'");
    }else{
      $tampil=mysql_query("SELECT * FROM tbl_user INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division WHERE tbl_user.nama LIKE '%$_POST[cari]%' AND tbl_division.id_division = '$_POST[id_division]'");
    }
    
    $no=1;
    while ($r=mysql_fetch_array($tampil)){
       echo "<tr><td align='center'>$no</td>
             <td>$r[nama]</td>
       <td align='center'>$r[email]</td>
             <td align='center'>
        <a class='btn btn-outline-danger' href=$aksi?module=users&act=hapus&id=$r[id_user] role='button'><i class='fa fa-trash mr-1'></i></a></td>";
        if ($r[statusaktif]=="Y") {
          echo"<td align='center'><input class='btn btn-outline-dark' type=button value='Non Aktifkan' onclick=\"window.location.href='$aksi?module=users&act=nonaktif&id=$r[id_user]';\"></td>";

        }else {
          echo"<td align='center'><input class='btn btn-success' type=button value='Aktifkan' onclick=\"window.location.href='$aksi?module=users&act=aktif&id=$r[id_user]';\"></td>";
        }
        
        echo"   </td>
    <td align='center'><a class='btn btn-outline-info' href=?module=users&act=lihat&id=$r[id_user] role='button'><i class='fa fa-eye mr-1'></i></a></td>

    </tr>";
      $no++;
    }
    echo "</table>";
    break;
}
?>

                        </div>
                    </div>
                    </div>    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->



    </div>
</div>
        