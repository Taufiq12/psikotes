<html lang="en" moznomarginboxes mozdisallowselectionprint>
<head>
    <title>Laporan Data Peserta Tes Online</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../../asset/css/bootstrap.min.css">
</head>
<body onload="window.print()">
<div class="container">
            <h1 class="text-right">PT. Bank Yudha Bhakti.Tbk</h1>
            <h3 class="text-right">DAFTAR NILAI PESERTA TES ONLINE</h3>
            <p class="text-right">Jl. Raya Pasar Minggu Kav 32 Pancoran Jakarta Selatan 12780 </p>
<hr style="border-block-start-width: 10px;"/>
</div>
<?php 
include '../../config/koneksi.php';
$_tgl = date('Y-m-d');
$tampil = mysql_query("SELECT nama,jk,email,benar,salah,kosong,score,keterangan,division 
FROM tbl_user 
INNER JOIN tbl_nilai ON tbl_user.id_user=tbl_nilai.id_user 
INNER JOIN tbl_division ON tbl_user.id_division=tbl_division.id_division 
WHERE tbl_nilai.tanggal='$_tgl' ORDER BY tbl_nilai.tanggal,tbl_nilai.benar DESC");
echo"
<div class='container'><table class='table mt-3'>
          <tr align='center'><th>No</th><th>Nama</th><th>Division</th><th>Jenis Kelamin</th><th>Benar</th><th>Salah</th><th>Kosong</th><th>Nilai</th><th>Hasil</th></tr>"; 
    $no=1;
    while ($r=mysql_fetch_array($tampil)){
       echo "<tr><td align='center'>$no</td>
       <td >$r[nama]</td>
       <td align='center'>$r[division]</td>
             <td>$r[jk]</td>
        <td align='center'>$r[benar]</td>
        <td align='center'>$r[salah]</td>
        <td align='center'>$r[kosong]</td>
        <td align='center'>$r[score]</td>
        <td align='center'>$r[keterangan]</td>
        </tr>";
      $no++;
    } ?>
    <!-- Menghitung rata2 nilai max dan min bray -->
    <?php 


        // Nilai Tinggi
        $datamaks = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
                                 FROM tbl_salah_jawab
                                 JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
                                 WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = '$_tgl'
                                 GROUP BY id_soal
                                 ORDER BY count(tbl_soal.id_soal) DESC
                                 LIMIT 1");
        
        $maks = mysql_fetch_array($datamaks);

        $datasoalmaks = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND id_soal= $maks[id_soal]");
        $soalmaks = mysql_fetch_array($datasoalmaks);

        // Nilai Rata-Rata
        $datarata = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
                                FROM tbl_salah_jawab
                                JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
                                WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = '$_tgl'
                                GROUP BY id_soal
                                ORDER BY count(tbl_soal.id_soal) ASC
                                LIMIT 1,2");
        $rata = mysql_fetch_array($datarata);

        // Nilai Rendah
        $datamin = mysql_query("SELECT tbl_soal.id_soal, count(tbl_soal.id_soal) as maks, tbl_soal.soal
                                FROM tbl_salah_jawab
                                JOIN tbl_soal on tbl_soal.id_soal = tbl_salah_jawab.id_soal
                                WHERE tbl_soal.aktif='Y' AND tbl_salah_jawab.tanggal = '$_tgl'
                                GROUP BY id_soal
                                ORDER BY count(tbl_soal.id_soal) ASC
                                LIMIT 1");
        $min = mysql_fetch_array($datamin);

        $datasoalmin = mysql_query("SELECT * FROM tbl_soal WHERE aktif='Y' AND id_soal= $min[id_soal]");
        $soalmin = mysql_fetch_array($datasoalmin);

       
        // Total Peserta
        $datapsrt = mysql_query("SELECT COUNT(score) as peserta FROM tbl_nilai WHERE tanggal = '$_tgl'");
        $count = mysql_fetch_array($datapsrt);
                                 
        ?>
    <tr>
        <td>Terbanyak Salah</td>
        <td><?= $soalmaks['soal']; ?></td>
    </tr>
    <tr>
        <td>Rata-Rata Salah</td>
        <td><?= $rata['soal']; ?></td>
    </tr>
    <tr>
        <td>Terkecil Salah</td>
        <td><?= $soalmin['soal']; ?></td>
    </tr>
    <tr>
        <td>Total Peserta</td>
        <td><?= $count['peserta'] ?></td>
    </tr>
    </table></div>
    <table class="mt-4" align="center" style="width:800px; border:none;margin-top:5px;margin-bottom:20px;">
    <tr>
        <td align="right">Jakarta, <?php echo date('d-M-Y')?></td>
    </tr>
    <tr>
        <td align="right"></td>
    </tr>
   
    <tr>
    <td><br/><br/><br/><br/></td>
    </tr>    
    <tr>
        <td align="right">( ........................................... )</td>
    </tr>
    <tr>
        <td align="center"></td>
    </tr>
</table>
<table align="center" style="width:800px; border:none;margin-top:5px;margin-bottom:20px;">
    <tr>
        <th><br/><br/></th>
    </tr>
    <tr>
        <th align="left"></th>
    </tr>
</table>
</body>
</html>