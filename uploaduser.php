<?php
$conn = mysqli_connect("localhost:3307","root","11223344","psikotes");
require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
    
  $tgl = date("Y-m-d");
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
  
  if(in_array($_FILES["file"]["type"],$allowedFileType)){

        $targetPath = 'uploads/'.$_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
        
        $Reader = new SpreadsheetReader($targetPath);
        
        $sheetCount = count($Reader->sheets());
        for($i=0;$i<$sheetCount;$i++)
        {
            
            $Reader->ChangeSheet($i);
            
            foreach ($Reader as $Row)
            {
          
                $username = "";
                if(isset($Row[0])) {
                    $username = mysqli_real_escape_string($conn,$Row[0]);
                }
                
                $password = "";
                if(isset($Row[1])) {
                    $password = mysqli_real_escape_string($conn,$Row[1]);
                }

                $nama = "";
                if(isset($Row[2])) {
                    $nama = mysqli_real_escape_string($conn,$Row[2]);
                }

                $jeniskelamin = "";
                if(isset($Row[3])) {
                    $jeniskelamin = mysqli_real_escape_string($conn,$Row[3]);
                }

                $email = "";
                if(isset($Row[4])) {
                    $email = mysqli_real_escape_string($conn,$Row[4]);
                }

                $telp = "";
                if(isset($Row[5])) {
                    $telp = mysqli_real_escape_string($conn,$Row[5]);
                }

                $alamat = "";
                if(isset($Row[6])) {
                    $alamat = mysqli_real_escape_string($conn,$Row[6]);
                }

                $division = "";
                if(isset($Row[7])) {
                    $division = mysqli_real_escape_string($conn,$Row[7]);
                }
                
                if (!empty($username) || !empty($email)) {

                    if($username != "User Name" && $password != "Password" && $nama != "Nama" && $jeniskelamin != "Jenis Kelamin" && $email != "Email" && $telp != "No Telp" && $alamat != "Alamat" && $division != "Divisi"){

                        $result = mysqli_query($conn, "select * from tbl_user where email = '".$email."' ");
                        $r=mysqli_fetch_array($result);
                        $rowcount=mysqli_num_rows($result); 
                        $encrypt = md5($password);

                        $query = mysqli_query($conn, "select * from tbl_division where division = '".$division."'");
                        $t=mysqli_fetch_array($query);

                        if($rowcount > 0){ 
                            echo $r[id_user];
                            $query = "update tbl_user set username = '".$username."',password = '".$encrypt."', nama = '".$nama."', jk = '".$jeniskelamin."', email = '".$email."', telp = '".$telp."', alamat = '".$alamat."', statusaktif = 'Y', stat_tes = '', id_division = '".$t[id_division]."' where id_user = '".$r[id_user]."'";
                            $result = mysqli_query($conn, $query);
                        }
                        else
                        { 
                            $query = "insert into tbl_user(username, password, nama, jk, email, telp, alamat, statusaktif, stat_tes, id_division) values('".$username."', '".$encrypt."', '".$nama."', '".$jeniskelamin."', '".$email."', '".$telp."', '".$alamat."', 'Y', '', '".$t[id_division]."')";
                            $result = mysqli_query($conn, $query);
                        }

                    
                        if (! empty($result)) {
                            $type = "success";
                            $message = "Excel Data Imported into the Database";
                            header('location:admin/media?module=users');
                            unlink($targetPath);
                        } else {
                            $type = "error";
                            $message = "Problem in Importing Excel Data";
                            header('location:admin/media?module=users');
                            unlink($targetPath);
                        }
                    }
                }
             }
        
         }
  }
  else
  { 
        $type = "error";
        $message = "Invalid File Type. Upload Excel File.";
        header('location:admin/media?module=users');
  }
}
?>
